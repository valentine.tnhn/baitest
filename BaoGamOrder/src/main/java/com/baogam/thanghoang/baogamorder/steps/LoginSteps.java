package com.baogam.thanghoang.baogamorder.steps;

import com.baogam.thanghoang.baogamorder.ComoonPageObject;
import com.baogam.thanghoang.baogamorder.pages.LoginPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;

public class LoginSteps extends ComoonPageObject {
    WebDriver driver = new ChromeDriver();


    public void openLoginPage() {
        openBrowser("https://baogam.order.gobizdev.com/#/login");
    }

    private void openBrowser(String s) {
    }


    public void enterUserName(String userName) {
        driver.findElement(By.xpath(LoginPage.XPATH_USER_NAME)).sendKeys(userName);
    }
    public void enterPassword(String password) {
        driver.findElement(By.xpath(LoginPage.XPATH_USER_NAME)).sendKeys(password);
    }

    public void verifyErrorMsgLogin(String status) {
        try {
            wait(5000);
            assertThat(String.format(LoginPage.XPATH_ERROR_MSG)).isEqualTo(status);
        } catch (InterruptedException e) {
            e.printStackTrace();
            getDriver().navigate().refresh();
            wait(5000);
            assertThat(String.format(LoginPage.XPATH_ERROR_MSG)).isEqualTo(status);
        }
    }

}
