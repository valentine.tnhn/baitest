package com.baogam.hoangthang.baogamorder;

import com.baogam.thanghoang.baogamorder.steps.LoginSteps;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;

public class LoginDefs {

    @Steps
    LoginSteps loginSteps;
    @Given("user login to Bao Gam dashboard with {string} and {string}")
    public void userLoginToBaoGamDashboardWithAnd(String userName, String password) {
        loginSteps.openLoginPage();
        loginSteps.enterUserName(userName);
    }

    @Then("verify login {string}")
    public void verifyLogin(String status) {
        if (!status.isEmpty()){
            loginSteps.verifyErrorMsgLogin(status);

        }
    }
}
