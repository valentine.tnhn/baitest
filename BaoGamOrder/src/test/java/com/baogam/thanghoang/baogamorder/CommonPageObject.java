package com.baogam.thanghoang.baogamorder;


import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;

public class CommonPageObject {

    public void openBrowser(String _url) {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        String baseUrl = "https://baogam.order.gobizdev.com/#/login";
        driver.get(baseUrl);
    }








    public void closeBrowser() {
        getDriver().close();
    }
}
