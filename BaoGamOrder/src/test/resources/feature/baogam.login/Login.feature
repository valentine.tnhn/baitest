Feature: Login to Bao Gam

  Scenario Outline: User login Bao Gam dashboard
    Given user login to Bao Gam dashboard with "<username>" and "<password>"
    Then verify login "<status>"
    Examples:
      | username | password | status |
      | email         | password      | status                                       |
      |               |               | Bạn cần nhập mật khẩu>Bạn cần nhập tài khoản |
      |               | 123456        | Bạn cần nhập tài khoản                       |
      | hoangthang    |               | Bạn cần nhập mật khẩu                        |
      | hoangthang    | 123456        | Tài khoản hoặc mật khảu không chính xác      |
      | thanghoang111 | 1234QWEqwe    | Tài khoản hoặc mật khảu không chính xác      |
      | thanghoang    | 1234QWEqwe!@# | Tài khoản hoặc mật khảu không chính xác      |
